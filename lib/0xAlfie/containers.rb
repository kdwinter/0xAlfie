# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    # Array of all submodules (they should all be containers).
    def self.all
      constants.map { |c| const_get(c) }
    end

    # Load a command container.
    def self.load(container)
      begin
        require_relative "./containers/#{container.to_s.underscore}.rb"
        ZxAlfie.bot.discord.include!(const_get(container))
      rescue LoadError => e
        ZxAlfie.bot.logger.warn("Failed to load container #{container} (#{e.message}).")
      end
    end
  end
end
