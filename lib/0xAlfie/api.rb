# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module API extend self
    def google
      @@google ||= Google::APIClient.new(
        key:                 ZxAlfie.config[:api][:google_api_key],
        authorization:       nil,
        application_name:    ZxAlfie::NAME,
        application_version: ZxAlfie::VERSION,
        user_agent:          "0xAlfie/0.4.0 google-api-ruby-client/0.8.7 Linux/5.15.6-zen2-1-zen (gzip)"
      )
    end

    def youtube
      @@youtube ||= google.discovered_api("youtube", "v3")
    end

    def customsearch
      @@customsearch ||= google.discovered_api("customsearch", "v1")
    end
  end
end
