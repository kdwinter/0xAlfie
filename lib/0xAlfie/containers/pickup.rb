# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module Pickup
      extend Discordrb::Commands::CommandContainer

      class PickupGroup
        attr_reader :max_size, :users, :creator

        def initialize(max_size, creator_id)
          @max_size = max_size
          @creator  = creator
          @users    = {creator_id => Time.now.to_i}
        end

        def add(user_id)
          @users ||= {}
          @users[user_id] = Time.now.to_i
        end

        def remove(user_id)
          @users ||= {}
          @users.delete(user_id)
        end

        def size
          @users ||= {}
          @users.keys.size
        end

        def full?
          size == @max_size
        end
      end

      DUMP_FILE = File.join(ZxAlfie.root, "groups.dump")

      def self.bootstrap
        default_hash = Hash.new { |h, k| h[k] = {} }

        if File.exist?(DUMP_FILE)
          @@groups = (Marshal.load(File.read(DUMP_FILE)) rescue default_hash)
        else
          @@groups = default_hash
        end
      end

      def self.cleanup
        f = File.open(DUMP_FILE, "w+")
        @@groups.default = nil
        f.write(Marshal.dump(@@groups))
        f.close
      end

      command(:creategroup,
        description:          "Create a pickup group.",
        usage:                "#{ZxAlfie.prefix}creategroup dota 5",
        help_available:       true,
        min_args:             2,
        max_args:             2,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, name, size|
        size = size.to_i
        if size < 2
          "Group size should be bigger than 1."
        else
          if @@groups[event.server.id].key?(name)
            "This group already exists. Use `!joingroup #{name}` to add thyself!"
          else
            @@groups[event.server.id][name] = PickupGroup.new(size, event.user.id)
            "Created **#{name}** group. I'll ping everyone once #{size} players have joined."
          end
        end
      end

      command(:disbandgroup,
        description:          "Disband an open pickupgroup.",
        usage:                "#{ZxAlfie.prefix}disbandgroup dota",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, name|
        if group = @@groups[event.server.id][name]
          if event.user.id == group.creator || event.user.id == ZxAlfie.owner_id
            @@groups[event.server.id].delete(name)
            "Group **#{name}** has been disbanded."
          else
            "You can't disband this group you :potato:."
          end
        else
          "This group doesn't exist."
        end
      end

      command(:groups,
        description:          "List all open groups.",
        usage:                "#{ZxAlfie.prefix}groups",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event|
        if @@groups[event.server.id].size > 0
          event << "**List of open groups:**"
          @@groups[event.server.id].each do |key, group|
            event << "**#{key}** with **#{group.size}/#{group.max_size}** players, created by **" \
              "#{ZxAlfie.bot.discord.user(group.creator).name}**."
          end
          nil
        else
          "No groups currently open."
        end
      end

      command(:groupstatus,
        description:          "Check the status of an open group.",
        usage:                "#{ZxAlfie.prefix}groupstatus dota",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, name|
        if group = @@groups[event.server.id][name]
          players = group.users.map { |id, t|
            "#{ZxAlfie.bot.discord.user(id).name} since #{Time.at(t).strftime("%d-%m-%Y %H:%M:%S")}"
          }.join(", ")

          "**#{name}** is currently at **#{group.size}/#{group.max_size}** players (*#{players}*)."
        else
          "This group doesn't exist. Use `!creategroup #{name} 10` to create it with for example 10 intended players."
        end
      end

      command(:joingroup,
        description:          "Join an open group.",
        usage:                "#{ZxAlfie.prefix}joingroup dota",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, name|
        if group = @@groups[event.server.id][name]
          if group.users.key?(event.user.id)
            "You're in this group already..."
          else
            group.add(event.user.id)

            players = group.users.map { |id, t| ZxAlfie.bot.discord.user(id).mention }.join(", ")

            if group.full?
              @@groups[event.server.id].delete(name)

              "**#{name}** is now full. Go, #{players}!"
            else
              "**#{name}** is currently at **#{group.size}/#{group.max_size}** players (*#{players}*)."
            end
          end
        else
          "This group doesn't exist. Use `!creategroup #{name} 10` to create it with for example 10 intended players."
        end
      end

      command(:leavegroup,
        description:          "Leave an open group.",
        usage:                "#{ZxAlfie.prefix}leavegroup dota",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, name|
        if group = @@groups[event.server.id][name]
          if group.users.key?(event.user.id)
            group.remove(event.user.id)

            players = group.users.map { |id, t|
              "*#{ZxAlfie.bot.discord.user(id).name} since #{Time.at(t).strftime("%d-%m-%Y %H:%M:%S")}*"
            }.join(", ")

            if players.size.zero?
              "**#{name}** is currently at **#{group.size}/#{group.max_size}** players."
            else
              "**#{name}** is currently at **#{group.size}/#{group.max_size}** players (#{players})."
            end
          else
            "You're not in this group, fool."
          end
        else
          "This group doesn't exist. Use `!creategroup #{name} 10` to create it with for example 10 intended players."
        end
      end
    end
  end
end
