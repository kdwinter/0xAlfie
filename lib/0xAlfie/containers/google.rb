# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module Google
      extend Discordrb::Commands::CommandContainer

      command(:search,
        description:          "Search google and return the first result.",
        usage:                "#{ZxAlfie.prefix}search some random shit",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages embed_links),
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.customsearch.cse.list, parameters: {
              q: search, cx: ZxAlfie.config[:api][:google_search_key], num: 1, safe: "off"
            }
          )
          if result = JSON.parse(result.response.body)["items"]
            result = result.first
            event.channel.send_message "#{result["title"]}: #{result["link"]}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue => e
          ZxAlfie.capture_exception(e)
          event.channel.send_message "Something went wrong searching your :shit:. Error was logged."
          nil
        end
      end

      command(:image,
        description:          "Search google images and return the first image.",
        usage:                "#{ZxAlfie.prefix}image ass",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages embed_links),
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.customsearch.cse.list, parameters: {
              q: search, cx: ZxAlfie.config[:api][:google_search_key], num: 1, safe: "off", searchType: "image"
            }
          )
          if result = JSON.parse(result.response.body)["items"]
            result = result.first
            event.channel.send_message "#{result["title"]}: #{result["link"]}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue => e
          ZxAlfie.capture_exception(e)
          event.channel.send_message "Something went wrong searching your :shit:. Error was logged."
          nil
        end
      end

      command(:youtube,
        description:          "Search for a youtube video.",
        usage:                "#{ZxAlfie.prefix}youtube some random vid",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages embed_links),
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.youtube.search.list, parameters: {
              part: "snippet", q: search, maxResults: 1, type: "video", safeSearch: "none"
            }
          )

          if result = result.data.items.first
            event.channel.send_message "https://youtube.com/watch?v=#{result.id.videoId}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue => e
          ZxAlfie.capture_exception(e)
          event.channel.send_message "Something went wrong trying to find your video. Error was logged."
          nil
        end
      end

    end
  end
end
