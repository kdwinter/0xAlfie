# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "addressable/uri"

module ZxAlfie
  module Containers
    module DuckDuckGo
      extend Discordrb::Commands::CommandContainer

      command(:search,
        description:          "Search duckduckgo and return the first result.",
        usage:                "#{ZxAlfie.prefix}search some random shit",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages embed_links),
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = Net::HTTP.get_response(URI.parse(
            "https://html.duckduckgo.com/html/?q=#{search}"
          ))
          result = Nokogiri::HTML(result.response.body).css(".result")

          if Array(result).size > 0
            title = result.first.css(".result__title").first.css("a").first.text
            url   = result.first.css(".result__url").first["href"]

            if url.start_with?("//duckduckgo.com/l")
              uri = Addressable::URI.parse(url)
              url = uri.query_values["uddg"]
            end

            event.channel.send_message "#{title}: #{url}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue => e
          ZxAlfie.capture_exception(e)
          event.channel.send_message "Something went wrong searching your :shit:. Error was logged."
          nil
        end
      end
    end
  end
end
