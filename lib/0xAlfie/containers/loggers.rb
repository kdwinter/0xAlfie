# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module Loggers
      extend Discordrb::EventContainer

      COMMAND_EXTRACT_RE = /\A#{ZxAlfie.prefix}([a-z0-9\-_]+)/i
      message(private: false) do |event|
        content = event.message.content
        details = [event.channel.id, event.user.id, event.server.id, Time.now.strftime("%Y-%m-%d %H:%M:%S")]

        ZxAlfie.bot.logger.info("#{event.server.name} #{event.channel.name} #{event.user.name}: #{content}")

        if content.start_with?(ZxAlfie.prefix)
          command = content.match(COMMAND_EXTRACT_RE)[1]
          ZxAlfie.bot.db.execute(
            "INSERT INTO bot_calls (command, channel_id, user_id, server_id, called_at) VALUES (?, ?, ?, ?, ?)",
            [command] + details
          )
        else
          ZxAlfie.bot.db.execute(
            "INSERT INTO messages (content, channel_id, user_id, server_id, sent_at) VALUES (?, ?, ?, ?, ?)",
            [content] + details
          )
        end
      end

      presence do |event|
        ZxAlfie.bot.db.execute("DELETE FROM seen WHERE user_id = ? AND server_id = ?", event.user.id, event.server.id)
        ZxAlfie.bot.db.execute(
          "INSERT INTO seen (user_id, server_id, status, seen_at) VALUES (?, ?, ?, ?)",
          [event.user.id, event.server.id, event.status.to_s, Time.now.strftime("%Y-%m-%d %H:%M:%S")]
        )
      end
    end
  end
end
