# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module CustomCommands
      extend Discordrb::Commands::CommandContainer

      def self.register_command(key, value, server_id)
        ZxAlfie.bot.discord.command(key.to_sym,
          help_available:       false,
          required_permissions: %i(send_messages),
          permission_message:   false
        ) do |event|
          next if event.server.id.to_i != server_id.to_i

          #event.message.delete if ZxAlfie.bot.discord.profile.on(event.server).permission?(:manage_messages, event.channel)
          event.channel.send_message(value)

          nil
        end
      end

      def self.bootstrap
        ZxAlfie.bot.db.execute("SELECT key, value, server_id FROM commands") do |row|
          register_command(row["key"], row["value"], row["server_id"])
        end
      end

      command(:customcommands,
        description:          "List all custom commands.",
        usage:                "#{ZxAlfie.prefix}customcommands",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: %i(send_messages)
      ) do |event|
        message = "**List of custom commands:**\n"
        message += ZxAlfie.bot.db.execute(
          "SELECT key, value, server_id FROM commands WHERE server_id = ? ORDER BY key", event.server.id
        ).map { |row| "`#{row["key"]}`" }.join(", ")

        event.channel.send_message(message)

        #ZxAlfie.bot.db.execute("SELECT key, value, user_id, server_id FROM commands ORDER BY key") do |row|
          #if user = ZxAlfie.bot.discord.user(row['user_id'])
          #  event << "`#{row['key']}`, added by **#{user.name}**"
          #else
          #  event << "`#{row['key']}`"
          #end
        #end

        nil
      end

      command(:addcommand,
        description:        "Add a custom command.",
        usage:              "#{ZxAlfie.prefix}addcommand help I won't help you.",
        help_available:     true,
        min_args:           2,
        permission_level:   ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message: "I can't let you."
      ) do |event, command, *text|
        key, value = command.downcase, text.join(" ")

        if ZxAlfie.bot.core_commands.include?(key.to_sym)
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            event.channel.send_message "Can't override a builtin command."
          end

        else
          # TODO: Don't allow overriding existing commands unless caller
          # has :moderator permission or above

          ZxAlfie.bot.db.execute("DELETE FROM commands WHERE key = ? AND server_id = ?", command, event.server.id)
          ZxAlfie.bot.db.execute(
            "INSERT INTO commands (key, value, server_id, user_id) VALUES (?, ?, ?, ?)",
            [key, value, event.server.id, event.user.id]
          )

          register_command(key, value, event.server.id)

          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            event.channel.send_message "Command `#{key}` has been registered."
          end
        end

        nil
      end

      command(:removecommand,
        description:        "Remove a custom command.",
        usage:              "#{ZxAlfie.prefix}removecommand help",
        help_available:     true,
        min_args:           1,
        max_args:           1,
        permission_level:   ZxAlfie.permission_from_identifier(:moderator).level,
        permission_message: "Can't let you do that."
      ) do |event, command|
        if ZxAlfie.bot.core_commands.include?(command.to_sym)
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            event.channel.send_message "Can't remove builtin commands."
          end

        elsif ZxAlfie.bot.db.get_first_row(
          "SELECT * FROM commands WHERE key = ? AND server_id = ?", command, event.server.id
        )
          ZxAlfie.bot.db.execute(
            "DELETE FROM commands WHERE key = ? AND server_id = ?", command, event.server.id
          )
          ZxAlfie.bot.discord.remove_command(command.to_sym)

          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            event.channel.send_message "Command `#{command}` has been removed."
          end
        end

        nil
      end
    end
  end
end
