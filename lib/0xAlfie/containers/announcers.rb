# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module Announcers
      extend Discordrb::EventContainer

      member_join do |event|
        unless event.user.current_bot?
          if ZxAlfie::Containers.const_defined?(:Users)
            ZxAlfie::Containers::Users.kicked_users.delete_if { |u| u.id == event.user.id }
          end

          channel = event.server.general_channel
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, channel)
            channel.send_message "**#{event.user.name}** joined the server. Welcome!"
          end
        end
      end

      member_leave do |event|
        unless event.user.current_bot?
          channel = event.server.general_channel

          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, channel)
            channel.send_message "**#{event.user.name}** left the server. Bye!"
          end
        end
      end
    end
  end
end
