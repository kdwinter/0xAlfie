# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Containers
    module Utilities
      extend Discordrb::Commands::CommandContainer

      command(:eval,
        description:          "Evaluate.",
        usage:                "#{ZxAlfie.prefix}eval ZxAlfie.bot.voice_enabled?",
        help_available:       false,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(:administrator).level,
        permission_message:   false
      ) do |event, *args|
        break if event.user.id != ZxAlfie.owner_id

        begin
          result = eval(args.join(" ")).inspect
          ZxAlfie.bot.logger.info "Eval result: #{result}"
          event.channel.send_message result
        rescue => e
          event.channel.send_message "Exception caught: #{e.message}"
        end

        nil
      end

      command(:calc,
        description:          "Basic arithmetic.",
        usage:                "#{ZxAlfie.prefix}calc 1+2",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, *args|
        args = args.join(" ")
        # dentaku is some random library that takes a string and evaluates
        # the math inside, so I don't have to write it myself
        begin
          result = Dentaku::Calculator.new.evaluate(args) || "I'm confused"
        rescue
          result = "fuck that shit"
        end

        event.channel.send_message "According to my calculations, **#{args}** = **#{result}**"

        nil
      end

      command(:urban,
        description:          "Search Urban Dictionary for a definition. Only the first match is returned.",
        usage:                "#{ZxAlfie.prefix}urban meme",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(send_messages embed_links),
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = Net::HTTP.get_response(URI.parse(
            "https://api.urbandictionary.com/v0/define?term=#{search}"
          ))
          result = JSON.parse(result.body)["list"].first

          message = "**#{search}** on Urban Dictionary:  "
          message << result["definition"]

          event.channel.send_message(message)
        rescue => e
          ZxAlfie.capture_exception(e)
          event.channel.send_message "Something went wrong. Error was logged."
        end

        nil
      end

      EIGHTBALL_ANSWERS = [
        "yes", "no", "outlook not so good", "all signs point to yes",
        "all signs point to no", "why the hell are you asking me?",
        "the answer is unclear"
      ].freeze.map(&:freeze)
      command(:eightball,
        description:          "Ask the magic eightball.",
        usage:                "#{ZxAlfie.prefix}eightball am I stupid",
        help_available:       true,
        min_args:             0,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, *question|
        event.channel.send_message "*shaking the magic 8-ball...* #{EIGHTBALL_ANSWERS.sample}"

        nil
      end

      command(:roll,
        description:          "Roll a random number.",
        usage:                "#{ZxAlfie.prefix}roll, #{ZxAlfie.prefix}roll 100",
        help_available:       true,
        min_args:             0,
        max_args:             1,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event, max|
        roll =
          if max
            rand(1..max.to_i)
          else
            rand(1..100)
          end

        event.channel.send_message "**#{event.user.name}** rolled **#{roll}** (1-#{(max || 100).to_i})."

        nil
      end

      command(:coinflip,
        description:          "Flip a coin.",
        usage:                "#{ZxAlfie.prefix}coinflip",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: %i(send_messages),
        permission_message:   false
      ) do |event|
        result = %w(Heads Tails).sample
        event.channel.send_message "**#{event.user.name}** flipped a coin and got **#{result}**."

        nil
      end

      command(:ddos,
        description:          "Fuck him up.",
        usage:                "#{ZxAlfie.prefix}ddos Bald",
        help_available:       true,
        min_args:             1,
        required_permissions: %i(speak manage_channels manage_server),
        permission_level:     ZxAlfie.permission_from_identifier(:moderator).level,
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        member = event.server.members.detect { |m| m.name.casecmp(name).zero? }

        if member && member.id != ZxAlfie.owner_id
          if channel = member.voice_channel
            3.times do
              event.server.move(member, event.server.channels.reject { |c| c.id == channel.id }.sample)
              sleep 0.5
              event.server.move(member, channel)
              sleep 0.5
            end

            nil
          else
            event.channel.send_message "**#{name}** is not in a voice channel."
          end
        else
          event.channel.send_message "Couldn't find **#{name}** in the server. Check spelling?"
        end

        nil
      end

    end
  end
end
