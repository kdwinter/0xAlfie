# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ZxAlfie
  module Configuration
    class << self
      @@configuration = nil

      def load_from_file
        @@configuration = ParseConfig.new(File.join(ZxAlfie.root, "bot.conf"))
      rescue
        ZxAlfie.bot.logger.error("Please make sure `bot.conf` exists and is readable.")
        exit 1
      end

      def values
        if @@configuration.nil?
          load_from_file
          @@configuration = deep_symbolize_keys(@@configuration.params)
        end

        @@configuration
      end

      # The bot won't run without some of its config variables being set,
      # this method will recursively check if given keys are present
      def assert_value_presence(*keys)
        cfg = values.dup

        keys.each do |key|
          val = cfg[key.to_sym]

          if val.nil? || val == "".freeze
            ZxAlfie.bot.logger.error("`#{keys.join(".")}' config value not set. This bot shall not run.")
            exit 1
          end

          # Go deeper down
          cfg = val
        end

        true
      end

      private def deep_symbolize_keys(hash)
        result = {}

        hash.each do |key, val|
          val = deep_symbolize_keys(val) if val.is_a?(Hash)
          result[key.to_sym] = val
        end

        result
      end
    end
  end
end
