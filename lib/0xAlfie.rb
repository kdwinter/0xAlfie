# 0xAlfie is discord bot using discordrb.
# Copyright (C) 2017 Kenneth De Winter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "open-uri"
require "singleton"
require "bundler"
Bundler.require

require_relative "0xAlfie/permission"

module ZxAlfie
  extend self

  NAME = "0xAlfie".freeze
  ROOT = File.dirname(File.expand_path(File.join(__FILE__, ".."))).freeze

  # The bot of bots.
  def bot
    Bot.instance
  end

  # The bot's root path.
  def root
    ROOT
  end

  # Holds configuration information.
  def config
    Configuration.values
  end

  # The Discord user ID of this bot's owner.
  def owner_id
    config[:bot][:owner_id].to_i
  end

  # Prefix used to trigger commands.
  def prefix
    config[:commands][:prefix]
  end

  # Available permission levels.
  PERMISSIONS = [
    Permission.new(1, :pleb,          "Pleb".freeze),
    Permission.new(2, :trusted_user,  "Trusted User".freeze),
    Permission.new(3, :moderator,     "Moderator".freeze),
    Permission.new(4, :administrator, "Administrator".freeze)
  ].freeze

  # Grab permission object by identifier.
  def permission_from_identifier(identifier)
    PERMISSIONS.detect { |p| p.identifier == identifier.to_sym }
  end

  # Grab permission object by level.
  def permission_from_level(level)
    PERMISSIONS.detect { |p| p.level == level.to_i }
  end

  # Shortcut
  def load_container(container)
    Containers.load(container)
  end

  def capture_exception(e)
    Discordrb.split_message(e.message).each do |chunk|
      bot.discord.user(owner_id).pm(chunk)
    end
  end
end

require_relative "0xAlfie/version"
require_relative "0xAlfie/bot"
require_relative "0xAlfie/configuration"
require_relative "0xAlfie/api"
require_relative "0xAlfie/containers"
